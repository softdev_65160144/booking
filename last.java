import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class last {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        String[] movies = {"Barbie", "MEG 2", "Mission Impossible", "Wittaya in wonderland", "Jurassic world"};
        String[][] timings = {
            {"10:00 AM", "02:00 PM", "06:00 PM"},
            {"11:00 AM", "03:00 PM", "07:00 PM"},
            {"12:00 AM", "04:00 PM", "05:00 PM"},
            {"6:00 AM", "06:00 PM", "09:00 PM"},
            {"1:00 AM", "10:00 PM", "12:00 PM"}
        };
        String[] soundtrack = {"TH", "ENG"};

        int numRows = 5;  
        int numCols = 8;  
        String[][] seats = initializeSeats(numRows, numCols);
        List<String> reservedSeats = new ArrayList<>();

        int selectedMovie = showOptions("Available Movies:", movies);
        int selectedTiming = showOptions("Available Timings:", timings[selectedMovie]);
        int selectedSoundtrack = showOptions("Available Soundtracks:", soundtrack);
        int totalPeople = selectPeople();
        int totalMembers = selectMembers(totalPeople);
        int loop = totalPeople;
    
        showSeparator();
        printSeats(seats);
        for (int i = 0; i < loop; i++) {
            System.out.print("Enter seat ID (e.g., A1) : ");
            String seatID = scanner.next();

            if (reserveSeat(seatID, seats)) {
                reservedSeats.add(seatID);
                System.out.println("Seat " + seatID + " has been reserved.");
                printSeats(seats);
                showSeparator();
            } else {
                System.out.println("Invalid seat selection. Please choose an available seat.");
                loop++;
            }
        }

        scanner.close();
        showBookingSummary(movies[selectedMovie], timings[selectedMovie][selectedTiming], soundtrack[selectedSoundtrack]);
        System.out.println("Reserved seats: " + reservedSeats);
        calculateTotalPrice(totalPeople, totalMembers);
        showSeparator();
    }

    static int showOptions(String message, String[] options) {
        System.out.println(message);
        showSeparator();

        Scanner scanner = new Scanner(System.in);
        int choice = -1;

        do {
            for (int i = 0; i < options.length; i++) {
                System.out.println((i + 1) + ". " + options[i]);
            }

            System.out.print("Select an option (1-" + options.length + "): ");
            try {
                choice = scanner.nextInt() - 1;

                if (choice < 0 || choice >= options.length) {
                    System.out.println("Invalid option. Please choose a valid option.");
                }
            } catch (Exception e) {
                System.out.println("Invalid input. Please enter a number.");
                scanner.next(); 
            }
        } while (choice < 0 || choice >= options.length);

        showSeparator();
        return choice;
    }

    static int selectPeople() {
        Scanner scanner = new Scanner(System.in);
        int numPeople = 0;
        boolean validInput = false;

        while (!validInput) {
            System.out.print("Enter the number of people (1-10): ");
            try {
                if (scanner.hasNextInt()) {
                    numPeople = scanner.nextInt();
                    if (numPeople >= 1 && numPeople <= 10) {
                        validInput = true;
                    } else {
                        System.out.println("Can't enter a number outside the range 1-10.");
                    }
                } else {
                    System.out.println("Invalid input. Please enter a valid number.");
                    scanner.next(); 
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.next(); 
            }
        }

        return numPeople;
    }

    static int selectMembers(int people) {
        Scanner scanner = new Scanner(System.in);
        int members = 0;
        boolean validInput = false;

        while (!validInput) {
            System.out.print("How many members are there? (1-" + people + "): ");
            try {
                if (scanner.hasNextInt()) {
                    members = scanner.nextInt();
                    if (members >= 1 && members <= people) {
                        validInput = true;
                    } else {
                        System.out.println("The number of members can't exceed the number of people or be less than 1.");
                    }
                } else {
                    System.out.println("Invalid input. Please enter a valid number.");
                    scanner.next(); 
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.next();
            }
        }

        return members;
    }

    static void calculateTotalPrice(int totalPeople, int totalMembers) {
        double ticketPrice = 150;
        double nonMembersPrice = (totalPeople - totalMembers) * ticketPrice;
        double membersPrice = (totalMembers * ticketPrice) * 0.90;
        double totalPrice = nonMembersPrice + membersPrice;
        System.out.println("Total Price: " + totalPrice + " Baht");
    }

    static void showBookingSummary(String movie, String timing, String soundtrack) {
        System.out.println("Booking Summary");
        System.out.println("Movie: " + movie);
        System.out.println("Timing: " + timing);
        System.out.println("Soundtrack: " + soundtrack);
        
    }
    
    static void showSeparator() {
        System.out.println("--------------------------");
    }
    
    static boolean reserveSeat(String seatID, String[][] seats) {
        for (int i = 0; i < seats.length; i++) {
            for (int j = 0; j < seats[i].length; j++) {
                if (seats[i][j] != null && seats[i][j].equalsIgnoreCase(seatID)) {
                    seats[i][j] = null;
                    return true;
                }
            }
        }
        return false;
    }

    public static void printSeats(String[][] seats) {
        System.out.println("Current seating arrangement:");
        for (String[] row : seats) {
            for (String seat : row) {
                if (seat == null) {
                    System.out.print("[X] ");
                } else {
                    System.out.print("[" + seat + "] ");
                }
            }
            System.out.println();
        }
    }

    public static String[][] initializeSeats(int numRows, int numCols) {
        String[][] seats = new String[numRows][numCols];
        char rowChar = 'A';
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                seats[i][j] = rowChar + Integer.toString(j + 1);
            }
            rowChar++;
        }
        return seats;
    }
}
